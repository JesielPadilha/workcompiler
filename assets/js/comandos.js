var executaComando = function (commandName, sentenca) {
    if (commandName == 'VA_PARA') {
        var x = sentenca.params.x;
        var y = sentenca.params.y;
        andar(x, y);
    }
    if (commandName == 'MARQUE') {
        marcarPosicaoAtual();
        $('.well').html('');
    }
    if (commandName == 'VIRAR') {
        direcao(sentenca.params);
    }
    if (commandName == 'DESMARQUE') {
        if (!desmarcarPosicaoAtual()) {
            $('.well').html('não há marca aqui.');
        } else {
            $('.well').html('');
        }
    }
    if (commandName == 'DESENHARL') {
        var x = sentenca.params.x;
        var y = sentenca.params.y;
        desenharLinha(x, y);
    }
    if (commandName == 'DESENHARC') {
        desenharCirculo(sentenca.params)
    }
    if (commandName == 'DESENHARP') {
        sentenca.params.forEach(function (ponto) {
            desenharLinha(ponto.x, ponto.y);
        });
        sentenca.extra_params.forEach(function (pontosP) {
            desenharLinha(pontosP.value.x, pontosP.value.y);
        });
    }
    if (commandName == 'ESCREVA') {
        var r = '';
        if (sentenca.params.nodeType == 'EXPRESSAO_ARITIMETICA') {
            r = getValorExpressao(sentenca.params);
            if (sentenca.params.result != undefined) {
                r += '=' + sentenca.params.result;
            }
        }

        if (sentenca.params.nodeType == 'EXPRESSAO_LOGICA') {
            if (sentenca.params.result != undefined) {
                sentenca.params.elements.forEach(function (elemento) {
                    r += elemento;
                });
                r += ' = ' + sentenca.params.result;
            } else {
                r = sentenca.params.name;
            }
        }

        if (sentenca.params.name == 'STRING') {
            r = sentenca.params.value;
        }
        $('.well').html(r);
    }
    if (commandName == 'MEMORIZE') {
        $('.well').html('<b>'
            + sentenca.params.var
            + '</b> = '
            + getValorExpressao(sentenca.params.expressao) + '.');
    }
}