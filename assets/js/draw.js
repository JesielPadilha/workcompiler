var objPath;
var obj;
var x;
var y;
var grau;
var cor;
var espessura;
var linha;
var tipoCaneta;
var arraySegmentos = [];

function iniciarObj(x, y) {
	var triangle = new paper.Path.RegularPolygon(new paper.Point(x, y), 3, 12);
	triangle.fillColor = getCorAtual();
	// triangle.fillColor = {
	// 	gradient: {
	// 		stops: [['yellow', 0.05], ['red', 0.5], ['black', 1]],
	// 		radial: true
	// 	},
	// 	origin: (triangle.position.x -= 5),
	// 	destination: triangle.bounds.rightCenter
	// };

	obj = triangle;
	obj.rotate(0);
}

function andar(x, y) {
	obj.position.x += Number(x);
	obj.position.y += Number(y);
}

function desenharLinha(x, y) {
	obj.position.x += Number(x);
	obj.position.y += Number(y);

	caminho(x, y);
}

function desenharCirculo(r) {
	var circulo = new paper.Path.Circle({
		radius: r,
		strokeColor: 'black',
		position: getPosicaoAtual()
	});
}

function marcarPosicaoAtual() {
	var marca = new paper.Path.Circle({
		position: getPosicaoAtual(),
		radius: 10,
		strokeColor: 'black',
		fillColor: 'red'
	});
	arraySegmentos.push(marca);
	objPath = marca;

	var layer = new paper.Layer({
		children: [marca, obj],
		position: getPosicaoAtual()
	});
}

function desmarcarPosicaoAtual() {
	arraySegmentos.forEach(function (marca) {
		if (marca.position.x == getPosicaoAtual().x && marca.position.y == getPosicaoAtual().y) {
			marca.removeSegments();
			return true;
		} else {
			return false;
		}
	});
}

function verificarMarcaPosicaoAtual() {
	var dep = jQuery.Deferred();
	arraySegmentos.forEach(function (marca) {
		if (marca.position.x == getPosicaoAtual().x && marca.position.y == getPosicaoAtual().y) {
			dep.resolve(true);
		} else {
			dep.resolve(false);
		}
	});
	return dep.promise();
}

function verificarMarcaPosicaoEspecifica(x, y) {
	var dep = jQuery.Deferred();
	arraySegmentos.forEach(function (marca) {
		if (marca.position.x == x && marca.position.y == y) {
			dep.resolve(true);
		} else {
			dep.resolve(false);
		}
	});
	return dep.promise();
}

function direcao(direcao) {
	if (direcao == 'norte') {
		if (grau != 0) {
			grau *= (-1);
			obj.rotate(grau);
			grau = 0;
		}
	}

	if (direcao == 'oeste') {
		if (grau != 90) {

			if (grau == 0)
				obj.rotate(90);

			else if (grau == 180)
				obj.rotate(-90);

			else
				obj.rotate(-180);

			grau = 90;
		}
	}

	if (direcao == 'sul') {
		if (grau != 180) {

			if (grau == 270)
				obj.rotate(-90);

			else if (grau == 90)
				obj.rotate(90);

			else
				obj.rotate(180);

			grau = 180;
		}
	}

	if (direcao == 'leste') {
		if (grau != 270) {

			if (grau == 0)
				obj.rotate(270);

			else if (grau == 90)
				obj.rotate(180);

			else
				obj.rotate(90);

			grau = 270;
		}
	}
}

function inserirTexto() {
	var text = new paper.PointText(new paper.Point(x, y));
	text.justification = 'center';
	text.fillColor = 'black';
	text.content = 'The contents of the point text';
}

function caminho(x, y) {
	var firstSegment = new paper.Segment({
		point: [obj.position.x, obj.position.y],
		handleOut: [0, 0]
	});

	var secondSegment = new paper.Segment({
		point: [(obj.position.x - x), (obj.position.y - y)],
		handleIn: [0, 0]
	});

	var path = new paper.Path({
		segments: [firstSegment, secondSegment],
		strokeColor: getCorAtual(),
		strokeWidth: getEspessuraAtual(),
		dashArray: getTipoCaneta()
	});
}

function getPosicaoAtual() {
	var posicaoAtual = new paper.Point(obj.position.x, obj.position.y);
	return posicaoAtual;
}

function mudarCorLinha(color) {
	cor = color;
}

function getCorAtual() {
	cor = (cor == undefined ? 'black' : cor);
	return cor;
}

function mudarEspessuraLinha(value) {
	espessura = value;
}

function getEspessuraAtual() {
	espessura = (espessura == undefined ? 1 : espessura);
	return espessura;
}

function mudarTipoCaneta(tipo) {
	if (tipo == 'normal') {
		tipoCaneta = [];
	} else {
		tipoCaneta = [10, 10];
	}
}

function getTipoCaneta() {
	tipoCaneta = (tipoCaneta == undefined ? [] : tipoCaneta);
	return tipoCaneta;
}