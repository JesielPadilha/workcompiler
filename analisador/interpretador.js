var interpretador = function (code) {
    // console.log('Retorno processamento: ', code);

    code.sentencas.forEach(function (sentenca) {

        //Comandos ==========================================
        if (sentenca.name == 'VA_PARA') {
            //va para (0, -100).
            var x = sentenca.params.x;
            var y = sentenca.params.y;
            andar(x, y);
        }
        if (sentenca.name == 'MARQUE') {
            // marque.
            marcarPosicaoAtual();
            $('.well').html('');
        }
        if (sentenca.name == 'VIRAR') {
            // vire para o sul.
            direcao(sentenca.params);
        }
        if (sentenca.name == 'DESMARQUE') {
            //desmarca a posição atual.
            if (!desmarcarPosicaoAtual()) {
                $('.well').html('não há marca aqui.');
            } else {
                $('.well').html('');
            }
        }
        if (sentenca.name == 'DESENHARL') {
            //desenhe uma linha no ponto (0, 100).
            var x = sentenca.params.x;
            var y = sentenca.params.y;
            desenharLinha(x, y);
        }
        if (sentenca.name == 'DESENHARC') {
            //desenhe um circulo com raio 100.
            desenharCirculo(sentenca.params)
        }
        if (sentenca.name == 'DESENHARP') {
            //quadrado:
            //desenhe um poligono passando por (0, -100), (100, 0), (0, 100), (-100, 0).
            //triangulo:
            // desenhe um poligono passando por (100, -100), (100, 100), (-200, 0).
            sentenca.params.forEach(function (ponto) {
                desenharLinha(ponto.x, ponto.y);
            });
            sentenca.extra_params.forEach(function (pontosP) {
                desenharLinha(pontosP.value.x, pontosP.value.y);
            });
        }
        if (sentenca.name == 'ESCREVA') {
            //escreva 1+1.
            //escreva 'a'=='a'.
            //escreva 'Teste'.
            var r = '';
            if (sentenca.params.nodeType == 'EXPRESSAO_ARITIMETICA') {
                r = getValorExpressao(sentenca.params);
                if (sentenca.params.result != undefined) {
                    r += '=' + sentenca.params.result;
                }
            }

            if (sentenca.params.nodeType == 'EXPRESSAO_LOGICA') {
                if (sentenca.params.result != undefined) {
                    sentenca.params.elements.forEach(function (elemento) {
                        r += elemento;
                    });
                    r += ' = ' + sentenca.params.result;
                } else {
                    r = sentenca.params.name;
                }
            }

            if (sentenca.params.name == 'STRING') {
                r = sentenca.params.value;
            }
            $('.well').html(r);
        }
        if (sentenca.name == 'MEMORIZE') {
            //memorize 1*5 em var.
            $('.well').html('<b>'
                + sentenca.params.var
                + '</b> = '
                + getValorExpressao(sentenca.params.expressao) + '.');
        }

        //Funções de ambiente ==================================
        if (sentenca.name == 'HMA') {
            //ha uma marca qui.
            verificarMarcaPosicaoAtual().then(function (result) {
                $('.well').html(result == true ? 'Verdadeiro' : 'Falso');
            });
        }
        if (sentenca.name == 'HME') {
            //ha uma marca em (250, 247).
            verificarMarcaPosicaoEspecifica(sentenca.value.x, sentenca.value.y).then(function (result) {
                $('.well').html(result == true ? 'Verdadeiro' : 'Falso');
            });
        }

        //Função de IO ========================================
        if (sentenca.name == 'INPUT') {
            //input 'hello world'
            $('.well').html('<b>entrada: </b>' + sentenca.value);
        }

        //Funções alteração da caneta ========================
        if (sentenca.name == 'MUDAR_COR') {
            //mude a cor da caneta para 'red'.
            var cor = sentenca.params.replace("'", '').replace("'", '');
            mudarCorLinha(cor);
        }
        if (sentenca.name == 'MUDAR_ESPESSURA') {
            //mude a espessura da caneta para 5.
            var value = sentenca.params;
            mudarEspessuraLinha(value);
        }
        if (sentenca.name == 'MUDAR_TIPO') {
            //mude o tipo da caneta para 'pontilhada'.
            mudarTipoCaneta(sentenca.params.replace("'", '').replace("'", ''));
        }

        //Funções de condicional ===============================
        if (sentenca.nodeType == 'CONDICIONAL') {
            // se 'a' == 'a' entao marque..
            // se 'a' == 'a' entao marque. senao desmarque..
            getValorConficional(sentenca.params[1].result).then(function (result) {
                if (result) {
                    if (sentenca.params[3].nodeType == 'COMANDO') {
                        executaComando(sentenca.params[3].name, sentenca.params[3]);
                    }
                } else {
                    executaComando(sentenca.params[5].name, sentenca.params[5]);
                }
            });
        }
    });
};

function getValorExpressao(expressao) {
    var r = '';
    if (expressao.nodeType == 'EXPRESSAO_ARITIMETICA' && expressao.name != 'NUMERO') {
        expressao.elements.forEach(function (elemento) {
            r += elemento;
        });
    }
    if (expressao.nodeType == 'EXPRESSAO_ARITIMETICA' && expressao.name == 'NUMERO') {
        r = expressao.value;
    }
    if (expressao.nodeType == 'EXPRESSAO_ARITIMETICA' && expressao.name == 'PAREN') {
        r = '(' + getValorExpressao(expressao.elements[1]) + ')';
    }
    return r;
}

function getValorConficional(expressao) {
    var def = jQuery.Deferred();
    if (expressao) {
        def.resolve(true);
    } else {
        def.resolve(false);
    }
    return def.promise();
}

